---
title: "CoinTracking - Keep Track of Your Crypto - Easily Create Tax Reports"
image: images/videos/055.jpg
date: 2018-03-11T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "cointracking", "taxes", "portfolio", "reviews"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/cointracking-keep-track-of-your-crypto/a5767cc35e4629aab5bb16b942a6bb780a9b8719?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is a quick review of CoinTracking.info, a website that helps you keep track of all your cryptocurrency transactions, trades, and mining income. You can manually import your transactions or automatically import them via API from all the major exchanges (annual subscription required for auto import). After you have all your trades in the system, you can easily generate FIFO tax reports for capital gains/losses with just a few clicks. Use the mobile app to keep an eye on your portfolio anywhere.

Show your support for the channel by signing up to CoinTracker.info today and get 10% off for upgrading to the Pro or Unlimited version:  
https://goo.gl/xHWVei