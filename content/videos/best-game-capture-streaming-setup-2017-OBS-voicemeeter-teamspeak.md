---
title: "Best Game Capture / Streaming Setup 2017 - OBS, Voicemeeter, & Teamspeak"
image: images/videos/005.jpg
date: 2017-04-16T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["streaming", "capture", "OBS", "voicemeeter", "teamspeak", "setup", "gaming"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/best-game-capture-streaming-setup-2017/cf086f9ca160c0fe62c811443190ed538ddf8a3e?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video, I show you how I use Open Broadcaster Studio, Voicemeeter Banana, and TeamSpeak to capture my desktop video and audio, for streaming or recording your games/desktop. 
 This process separates audio from multiple sources, e.g. desktop, microphone, teamspeak, and records each to its own separate track.  This is helpful to get the best audio mix for your gaming or desktop capture session, which can be accomplished with video editing software post-production.

Open Source Software used:  
http://vb-audio.pagesperso-orange.fr/Voicemeeter/banana.htm  
https://obsproject.com/  
https://www.teamspeak3.com/teamspeak-download.php  