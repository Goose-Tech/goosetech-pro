---
title: "Crypto Mining Difficulty 101 - Everything You Need to Know"
image: images/videos/062.jpg
date: 2018-03-21T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "difficulty", "101"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/crypto-mining-difficulty-101-everything/462f46207671d4d37ed8c0bfc2c725107b368a14?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video, I attempt to describe how crypto mining difficulty works and how it affects profitability. I also crunch some numbers to show alternative methods for determining profits based on individual hashrate and pool combined hashrate.

Helpful Difficulty & Luck Articles:  
http://www.itpro.co.uk/digital-currency/30249/what-is-cryptocurrency-mining

https://2miners.com/blog/mining-mining-luck/

https://www.bitcoinmining.com/what-is-bitcoin-mining-difficulty/