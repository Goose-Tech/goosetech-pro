---
title: "Advanced CPU Mining Guide - Intel or AMD CPU Mining with Awesome Miner & Mining Pool Hub"
image: images/videos/034.jpg
date: 2018-01-22T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["mining", "CPU", "Awesome Miner", "tutorial", "advanced"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/advanced-cpu-mining-guide-intel-or-amd/875e0c559e2e04e0db2288996ee361e817015edc?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This is an advanced tutorial for mining with a CPU using Awesome Miner and Mining Pool Hub. I go over all the considerations, the files, and the setup to get the best hashrate out of your CPU. The examples used are for Monero and Electroneum, two coins using the CryptoNight algorithm, which is better suited for CPU mining.