---
title: "ZelNode Setup Guide - Episode 3 of 3 - Registering Apps on Flux"
image: images/videos/zelnode-setup-ep3.jpg
date: 2020-09-21T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "ZEL", "zelcore", "wallet", "zelnode", "guide", "flux"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/zelnode-setup-guide-episode-3-of-3/8dd3b1c9f2f39b4fc4b8a3b68f077f10e265f782?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is episode 3 of my series on setting up and using a ZelNode. In this video, I show how to register an application on Flux, the gateway to the Zel network. Use Flux to run your apps for a fraction of the cost of VPS rentals.

📘Table of Contents:  
◆ 00:08 Intro  
◆ 02:23 Things to Consider First  
◆ 06:21 ZelCore+ ZelNode App  
◆ 07:44 Registration Process  
◆ 15:10 Dibifetch Running on Flux  
◆ 15:40 Final Thoughts  
●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬●  
⭐Links:  
💻 Vultr: https://www.vultr.com/?ref=8488046-6G  

💻 Contabo: https://contabo.com  

💻 Zel Website: https://www.zel.network  

💻 Dibifetch Website: https://markets.dibifetch.com  
