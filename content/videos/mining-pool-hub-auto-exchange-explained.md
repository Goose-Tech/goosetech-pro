---
title: "Mining Pool Hub Auto Exchange Explained"
image: images/videos/018.jpg
date: 2017-12-25T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "miningpoolhub.com", "auto exchange"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/mining-pool-hub-auto-exchange-explained/0b9376ce5c17b6cc84def90e71b823221d4149f6?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

Merry Christmas! In this video I explain how MPH's Auto Exchange feature works and how to read the Balances page. I also go into detail about how to determine transaction fee percentages for payouts.  I even sing a little at the end. Enjoy!