---
title: "ZelNode Setup Guide - Episode 1 of 3 - Preparing to Run a ZelNode"
image: images/videos/zelnode-setup-ep1.jpg
date: 2020-08-07T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "ZEL", "zelcore", "wallet", "zelnode", "guide"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/zelnode-setup-guide-episode-1-of-3/0e57a835a38b2bab2faf9a5679b6c278ca33469c?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This is part of a series of videos showing how to setup a ZelNode. In this video, I talk about the requirements, offer some suggestions for VPS providers, and walk through the process of setting up a Full Node within ZelCore.

Part 2 will detail how to deploy a VPS, login and install the ZelNode software, sync the blockchain, and start the node.

Part 3 will show how to login to Flux, register, and deploy applications on the Zel network.

📘Table of Contents:  
◆ 00:08 Intro  
◆ 01:50 MasterNode.Online Listing  
◆ 03:12 Different Tiers of ZelNodes  
◆ 03:30 ZelNodes Requirements & ZelBench  
◆ 05:20 VPS Providers (Vultr & Contabo)  
◆ 11:29 Determine Which Tier to Run  
◆ 12:13 Install ZelCore & Register Account  
◆ 14:40 Preview of ZelCore (d2fa, assets, portfolio)  
◆ 16:33 Purchase ZEL with CoinSwitch in ZelCore  
◆ 19:40 Start ZEL Full Node Wallet  
◆ 22:00 Create Transparent Address for Stake  
◆ 24:11 Send Collateral to Full Node Address  
◆ 27:35 Final Thoughts  
●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬●  
⭐Links:  
💻 Vultr: https://www.vultr.com/?ref=8488046-6G  

💻 Contabo: https://contabo.com  

💻 Zel Website: https://www.zel.network  

💻 Zel Full Node Bootstrap Guide: https://youtu.be/oiiDxW2r6m4  