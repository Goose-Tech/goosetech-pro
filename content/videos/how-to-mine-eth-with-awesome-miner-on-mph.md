---
title: "Ep02 - How to Mine Ethereum with Awesome Miner & Mining Pool Hub"
image: images/videos/coins/002.jpg
date: 2018-01-07T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "awesome miner", "miningpoolhub.com", "ETH"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/how-to-mine-ethereum-with-awesome-miner/7c97bc9aa70bf7367e450fb47cec3a24bd334259?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is the second in a series of videos showing you how to mine a single coin with Awesome Miner and Mining Pool Hub. Episode 02 is Ethereum.

This method is not profit switching.  For profit switching setup, please refer to this video: https://youtu.be/Tnw4zSD7TtA

Please be sure to adjust the GPU core clock and power settings accordingly to avoid damaging your GPU. Follow this video to adjust your settings: https://youtu.be/Nqqem-RQP6M

Links:  
Download AwesomeMiner: https://awesomeminer.com/download  

Join MiningPoolHub:
https://miningpoolhub.com/index.php?page=register  

Ethereum Wallet:
https://www.ethereum.org/