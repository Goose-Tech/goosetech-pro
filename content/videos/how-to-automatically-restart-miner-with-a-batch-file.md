---
title: "How to Automatically Restart Miner with a Batch File"
image: images/videos/101.jpg
date: 2018-05-28T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "batch file", "script", "mining"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/how-to-automatically-restart-miner-with/3c84f5d8bb19e9e7e08b6bf26f9de8375ba83949?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This simple mining life hack will help you keep your miner up and running 24/7 if it encounters a problem and shuts down. If the miner shuts down, the batch file loop simply restarts the mining engine.

📘Table of Contents:  
◆ 00:08 Intro  
◆ 00:33 Create Batch File Loop  
◆ 03:49 Final Thoughts  