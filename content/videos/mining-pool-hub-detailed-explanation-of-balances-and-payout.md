---
title: "Mining Pool Hub Detailed Explanation of Balances & Payout"
image: images/videos/025.jpg
date: 2018-01-02T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "miningpoolhub.com", "auto exchange"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/mining-pool-hub-detailed-explanation-of/a41f93e7f8fd6dd3a9d99f242b9e1c92b9d59e4d?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video I provide a more detailed look at Mining Pool Hub's Auto Exchange and the mysterious Balances page.  This is everything you need to know to set up MPH to payout in the coin of your choosing.

Special bonus is a new website that uses your MPH API key to display more detailed stats about your balances.

Check it out here: https://miningpoolhubstats.com/index.php  
