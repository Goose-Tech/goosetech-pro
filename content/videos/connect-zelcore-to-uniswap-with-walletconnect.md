---
title: "Connect ZelCore Wallet to UniSwap with WalletConnect"
image: images/videos/connect-zelcore-to-uniswap-walletconnect.jpg
date: 2020-09-19T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "ZEL", "zelcore", "wallet", "review", "zelnode", "guide", "flux"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/connect-zelcore-wallet-to-uniswap-with/889708adbc95216b0ccb67d983c0e1a046a45231?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video, I show how easy it is to connect ZelCore to UniSwap to trade directly from your ZelCore Wallet. Simply scan the QR code and start trading! 
@ZelLabs

📘Table of Contents:  
◆ 00:08 Intro  
◆ 00:41 Connect ZelCore to UniSwap  
◆ 02:21 Simple Trade  
◆ 05:26 Excitement & Glee  
◆ 06:10 Disconnect Wallet  
◆ 06:29 Final Thoughts  
●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬●  
⭐Links:  
💻 Zel Website: https://zel.network  

💻 UniSwap: https://app.uniswap.org  

