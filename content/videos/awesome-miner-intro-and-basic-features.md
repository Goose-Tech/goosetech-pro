---
title: "Awesome Miner - Intro & Basic Features - Mine Crypto with Ease"
image: images/videos/047.jpg
date: 2018-02-19T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "Awesome Miner", "tutorial"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/awesome-miner-intro-basic-features-mine/6abf9b84a9b5592ce59c0ad4f3c563af6b0e8251?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is an introduction to Awesome Miner, a one-stop software solution for mining cryptocurrency with a GPU, CPU, or ASIC. The software provides the ability to monitor and manage multiple mining rigs and, best of all, uses profit switching to ensure that each miner is mining the most profitable coins.