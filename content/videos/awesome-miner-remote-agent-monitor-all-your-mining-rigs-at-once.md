---
title: "Awesome Miner Remote Agent - Monitor All Your Mining Rigs at Once - Advanced Tutorial"
image: images/videos/056.jpg
date: 2018-03-13T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "Awesome Miner", "Remote Agent", "tutorial"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/awesome-miner-remote-agent-monitor-all/7c81d20221a854aaf95ebceb2dfcff74d751d33c?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

An advanced tutorial on how to configure Awesome Miner to control and monitor another mining computer using the Remote Agent. This may be an ideal setup for miners who have more than one mining rig and want to control them more easily through a single computer rather than making changes to each machine individually.

Join MiningPoolHub:  
https://goo.gl/PbTw6J

Download Awesome Miner:  
https://goo.gl/4u1WVc