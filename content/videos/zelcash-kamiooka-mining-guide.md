---
title: "ZelHash v1.0 Mining Guide - Kamiooka Network Upgrade is Here"
image: images/videos/zelcash-kamiooka-mining-guide.png
date: 2019-01-01T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "ZEL", "mining", "guide"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/zelhash-v1-0-mining-guide-kamiooka/b9531bd85e6e3f027a0841f82a3f74460b1e448c?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video, learn how to configure one of several different types of software to mine the new ZelHash algorithm.

📘Table of Contents:  
◆ 00:04 Intro  
◆ 01:29 Suggested Mining Engines  
◆ 03:13 Update Existing Setup on Awesome Miner  
◆ 03:49 Setting Up New Miner on Awesome Miner  
◆ 09:36 CLI Mining Setup  
◆ 11:48 ZelCore Features  
●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬●  
⭐Links:  
💻Zel Website: https://zel.network  

💻Medium Post on Kamiooka: https://medium.com/@ZelOfficial/zel-network-upgrade-kamiooka-v3-2-0-testnet-live-bf39bac9a5dd  

💻Awesome Miner: https://awesomeminer.com/download   

💻lolminer: https://github.com/Lolliedieb/lolMiner-releases/releases  

💻miniZ miner: https://miniz.ch/download/  

💻ZelNode Dashboard: https://dashboard.zel.network  


