---
title: "Crypto Mining w/RX480 on an ESXi 6.0 Server VM using Passthrough"
image: images/videos/007.jpg
date: 2017-11-26T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "esxi", "vmware", "server", "VM", "RX480"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/crypto-mining-w-rx480-on-an-esxi-6-0/0aec1bfca0ac3607771e136760d55d1b0321ed4e?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

I installed an RX480 in my VMware ESXi home server and put it to work mining Ethereum Classic.  I also talk about nanopool vs ethermine and some other stuff.  Enjoy!

HDMI Emulator Dongle (to simulate being connected to monitor):
https://goo.gl/9cDKj5  