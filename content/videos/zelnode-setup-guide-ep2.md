---
title: "ZelNode Setup Guide - Episode 2 of 3 - Deploy VPS, Install ZelNode, Start, & Monitor"
image: images/videos/zelnode-setup-ep2.jpg
date: 2020-08-09T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "ZEL", "zelcore", "wallet", "zelnode", "guide", "flux"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/zelnode-setup-guide-episode-2-of-3/85ba7badc84788bb0e5112dc20ab68214263e155?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is part 2 of my series on how to setup a ZelNode. Here, you will learn how to deploy a VPS, login and install the ZelNode software, sync the blockchain, and start the node. I also show some of the helpful tools available for monitoring your node after it's setup.

💻Commands:  
ZelNode Install Script: https://github.com/XK4MiLX/zelnode  

📘Table of Contents:  
◆ 00:08 Intro  
◆ 00:44 Choose & Setup VPS  
◆ 05:30 Login to VPS  
◆ 07:28 Install ZelNode  
◆ 11:09 Setup ZelCore Full Node Wallet  
◆ 18:15 Install Complete / Start ZelNode  
◆ 19:50 Login to Flux  
◆ 21:40 Admin Tools in Flux  
◆ 24:00 Monitoring with ZelCore+  
◆ 27:39 Final Thoughts  
●▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬●  
⭐Links:  
💻 Vultr: https://www.vultr.com/?ref=8488046-6G  

💻 Contabo: https://contabo.com  

💻 Zel Website: https://www.zel.network  