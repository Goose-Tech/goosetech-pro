---
title: "Triple Boot - Windows 10, Ubuntu, & Linux Mint"
image: images/videos/004.jpg
date: 2016-02-13T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["ubuntu", "triple boot", "windows 10", "linux mint"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/triple-boot-windows-10-ubuntu-linux-mint/2a7642f69ccf09a2c3f3cf5efaa48ddf3622f73b?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

In this video I show how to do a triple boot with Windows 10, Ubuntu 14.04.3, and Linux Mint 17.3.  This method maintains Windows 10 as the primary OS as well as keeping the Windows boot loader intact.  The Linux distros are installed in logical partitions, separate from Windows, with the GRUB boot loader installed to its own boot partition and a shared home partition.  Finally, EasyBCD is used to create an entry for Linux in the Windows boot loader.

Note:  This video assumes that you have already installed Windows 10.

Download Windows 10 - https://www.microsoft.com/en-us/software-download/windows10  
Download Ubuntu Desktop - http://www.ubuntu.com/download/desktop  
Download Linux Mint - http://www.linuxmint.com/download.php  
Download EasyBCD - http://neosmart.net/EasyBCD/  
Download Rufus - https://rufus.akeo.ie/  