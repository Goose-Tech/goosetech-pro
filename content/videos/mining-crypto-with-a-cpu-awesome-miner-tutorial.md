---
title: "Mining Crypto With a CPU - Awesome Miner Tutorial"
image: images/videos/021.jpg
date: 2017-12-29T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["mining", "CPU", "Awesome Miner", "tutorial"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/mining-crypto-with-a-cpu-awesome-miner/2cb42d5de0fbb2f3449a114f7f4957ceab25e446?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

New to crypto?  Don't have a GPU?  No worries!  This video will show you how to start mining one of the newest cryptocurrencies, Electroneum, with only your CPU*.  Anyone with a desktop PC and Windows can do this!

*CPU mining can cause the temperature of your CPU to rise.  Please be sure there is adequate cooling to prevent any permanent damage to your equipment.

Steps to get started mining ETN with your CPU:  
1. Start a new Electroneum wallet:  
https://my.electroneum.com

2. Register at Mining Pool Hub:  
https://miningpoolhub.com/index.php?page=register  

3. Download Awesome Miner:  
http://www.awesomeminer.com/download.aspx  

4. Follow instructions in the video.