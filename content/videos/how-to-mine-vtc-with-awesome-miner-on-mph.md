---
title: "Ep01 - How to Mine Vertcoin with Awesome Miner & Mining Pool Hub"
image: images/videos/coins/001.jpg
date: 2018-01-06T12:00:00-05:00
draft: false
type: "videos"
author: Goose-Tech
tags: ["crypto", "mining", "awesome miner", "miningpoolhub.com", "VTC"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/how-to-mine-vertcoin-with-awesome-miner/baa61d63b988587a1dee4d3a149d801526d037de?r=3qFKmEQLZkPFW7SEU9fMeytdY8akeVdt" allowfullscreen></iframe>

&nbsp;

#### DESCRIPTION

This video is the first in a series of videos showing you how to mine a single coin with Awesome Miner and Mining Pool Hub. Episode 01 is Vertcoin.

This method is not profit switching. For profit switching setup, please refer to this video: https://youtu.be/Tnw4zSD7TtA

Please be sure to adjust the GPU core clock and power settings accordingly to avoid damaging your GPU. Follow this video to adjust your settings: https://youtu.be/Nqqem-RQP6M

nVidia GPU users, if you having trouble connecting, try adding "stratum+tcp://" to the beginning of the server address.

Links:  
Download AwesomeMiner:  
http://www.awesomeminer.com/download.aspx  

Join MiningPoolHub:  
https://miningpoolhub.com/index.php?page=register  

Vertcoin Wallet:  
https://vertcoin.org/  

Electrum Wallet for Vertcoin:  
https://electrum.vertcoin.org/  